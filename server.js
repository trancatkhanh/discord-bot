const express = require('express')
const cors = require('cors')
const csurf = require('csurf')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const { config } = require('./config/config')
const appRouter = require('./config/routes')

const app = express()
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(bodyParser.raw())
app.use(cookieParser());

// Default path
app.get('/', (req, res) => {
  res.json({ message: 'dogrun-backend' });
});

app.use('/api', appRouter)

app.listen(config.SERVER_PORT, () => {
  console.log('Server is running on PORT', config.SERVER_PORT, config.NODE_ENV)
})

module.exports = app

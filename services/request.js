const axios = require('axios');

module.exports = async (method, url, data) => {
  const options = {
    url, method, data,
    headers: { 'content-type': 'application/x-www-form-urlencoded' },
    responseType: 'json'
  }

  const resultRequest = await axios(options)
  return resultRequest.data
}

const Joi = require('joi');
const {
  validateEncryptedMemberId,
  validateDecryptedMemberId
} = require('../helpers/member');

const validate = {

  createOrder: (payload) => {
    const createOrder = Joi.object({
      cainz_card_number: Joi.string()
        .required()
        .custom(validateEncryptedMemberId, 'custom validation'),
      dogrun_number: Joi.string().required(),
      order_start_time: Joi.string().required(),
      pets_id: Joi.string().required()
    })
    return createOrder.validate(payload)
  },

  cancelOrder: (payload) => {
    const cancelOrder = Joi.object({
      cainz_card_number: Joi.string()
        .required()
        .custom(validateEncryptedMemberId, 'custom validation'),
      order_sfid: Joi.string().required()
    })
    return cancelOrder.validate(payload)
  },

  getOrderInfo: (payload) => {
    const getOrderInfo = Joi.object({
      cainz_card_number: Joi.string()
        .required()
        .custom(validateEncryptedMemberId, 'custom validation')
    })
    return getOrderInfo.validate(payload)
  },

  checkRegistration: (payload) => {
    const checkRegistration = Joi.object({
      cainz_card_number: Joi.string()
        .required()
        .custom(validateEncryptedMemberId, 'custom validation')
    })
    return checkRegistration.validate(payload)
  },

  getOrderSituation: (payload) => {
    const getOrderSituation = Joi.object({
      store_code: Joi.string().required(),
      dogrun_shop_cd: Joi.string().required()
    })
    return getOrderSituation.validate(payload)
  },

  checkMemberId: (payload) => {
    const checkMemberId = Joi.object({
      cainz_card_number: Joi.string()
        .required()
        .custom(validateDecryptedMemberId, 'custom validation')
    })
    return checkMemberId.validate(payload)
  },

  decryptMemberId: (payload) => {
    const decryptMemberId = Joi.object({
      cainz_card_number: Joi.string()
        .required()
        .custom(validateEncryptedMemberId, 'custom validation')
    })
    return decryptMemberId.validate(payload)
  }
}

module.exports = validate

const { config } = require('./config/config')
const moment = require('moment')
const Discord = require('discord.js');
const client = new Discord.Client();

const db = require('diskdb');
db.connect('./data', ['users']);
const Users = db.users;

const commandPrefix = "!";
const botId = '<@!771178924427706428>'

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', async(msg) => {
    const args = msg.content.slice(commandPrefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();
    const authorId = msg.author.id
    const authorUserName = msg.author.username
    const userIsBot = msg.author.bot

    const getUser = Users.find({userId: authorId})
    if (getUser.length === 0) {
        const dataSave = {
            userId: msg.author.id,
            userName: msg.author.username,
            knb: 0,
            thanbinh: 0,
            giap: 0,
            pet: 0,
            level: 0,
            theluc: 1000,
            expiredTime: moment()
        }
        await Users.save(dataSave)
    }
    if(!msg.content.startsWith(commandPrefix)) {
        if (msg.content.toLowerCase().includes('chào hân')
            || msg.content.toLowerCase().includes('chào ' + botId)
            || msg.content.toLowerCase().includes('hi hân')
            || msg.content.toLowerCase().includes('hi ' + botId)
            || msg.content.toLowerCase().includes('hello hân')
            || msg.content.toLowerCase().includes('hello ' + botId)
        ) {
            return msg.reply('Chào thống lĩnh ạ');
        }

        if (msg.content.toLowerCase().includes('đúng ko hân')
            || msg.content.toLowerCase().includes('đúng ko ' + botId)
            || msg.content.toLowerCase().includes('đúng không hân')
            || msg.content.toLowerCase().includes('đúng không ' + botId)
            || msg.content.toLowerCase().includes('đúng hông hân')
            || msg.content.toLowerCase().includes('đúng hông ' + botId)
            || msg.content.toLowerCase().includes('phải ko hân')
            || msg.content.toLowerCase().includes('phải ko ' + botId)
            || msg.content.toLowerCase().includes('phải không hân')
            || msg.content.toLowerCase().includes('phải không ' + botId)
        ) {
            return msg.reply('dạ đúng rồi ạ');
        }

        return
    }


    console.log('getUser', getUser.length)

    const randomNumber = Math.floor(Math.random() * 100);

    const user = getUser[0]
    const currentTime = moment()

    const currentKnb = user.knb
    const isExpired = moment(currentTime).isAfter(user.expiredTime)

    const currentLevel = user.level
    const nextLevel = parseInt(currentLevel) + 1
    const levelAmount = config.level[nextLevel].knb
    const levelSuccess = config.level[nextLevel].tile

    const currentGiap = user.giap
    const nextGiap = parseInt(currentGiap) + 1
    const giapAmount = config.giap[nextGiap].knb
    const giapSuccess = config.giap[nextGiap].tile

    const currentPet = user.pet
    const nextPet = parseInt(currentPet) + 1
    const petAmount = config.pet[nextPet].knb
    const petSuccess = config.pet[nextPet].tile

    const currentThanbinh = user.thanbinh
    const nextThanbinh = parseInt(currentThanbinh) + 1
    const thanbinhAmount = config.thanbinh[nextThanbinh].knb
    const thanbinhSuccess = config.thanbinh[nextThanbinh].tile

    switch(command){
        case "ping":
            msg.reply('Pong!').then();
            break;
        case "fake":
            const fakeContent = msg.content.replace('!fake ', '')
            if (msg.channel.id === config.TEST_BOT_CHANNEL_ID) {
                client.channels.cache.get(config.TEST_BOT_CHANNEL_ID).send(fakeContent)
            }
            break;
        case "lixi":
            if (!isExpired) {
                const nextRound = moment(user.expiredTime).locale('vi').fromNow()
                return msg.reply(`Lần lì xì tiếp theo còn [${nextRound}]`).then();
            }
            const expiredTime = moment().add(30, 'minutes')
            const updatedKnb = randomNumber + parseInt(currentKnb)
            Users.update({userId: authorId}, {knb: updatedKnb, expiredTime})
            msg.reply(`Chúc mừng thống lĩnh nhận ${randomNumber} <:knb:771216025302728736>\n Hiện tại có ${updatedKnb} <:knb:771216025302728736>`).then();
            break;
        case "knb":
            msg.reply(`Hiện tại thống lĩnh có: ${user.knb} <:knb:771216025302728736>`).then()
            break;
        case "level":
            msg.reply(`Cấp hiện tại của thống lĩnh là [${user.level}]`).then()
            break;
        case "thanbinh":
            msg.reply(`Thần binh hiện tại của thống lĩnh cấp [${user.level}]`).then()
            break;
        case "giap":
            msg.reply(`Hiện tại thống lĩnh có giáp [${user.giap}]`).then()
            break;
        case "pet":
            msg.reply(`Pet hiện tại thống lĩnh đạt cấp [${user.pet}]`).then()
            break;
        case "uplevel":
            if (levelAmount > currentKnb) return msg.reply(`Thống lĩnh không đủ knb (thiếu ${levelAmount - parseInt(currentKnb)})`).then();
            if (randomNumber > levelSuccess) {
                const updateKnb = user.knb - levelAmount
                Users.update({userId: authorId}, {knb: updateKnb})
                return msg.reply('Nâng cấp thất bại, xin chia buồn cùng thống lĩnh')
            }

            const uplevel = Users.update({userId: authorId}, {level: nextLevel, knb: user.knb - levelAmount})
            msg.reply(`Chúc mừng thống lĩnh nâng cấp thành công, level hiện tại là [${nextLevel}]`).then()
            break;

        case "upgiap":
            if (nextGiap > currentLevel) return msg.reply('Không thể nâng vượt cấp của thống lĩnh')
            if (giapAmount > currentKnb) return msg.reply(`Thống lĩnh không đủ knb (thiếu ${giapAmount - parseInt(currentKnb)})`).then();
            if (randomNumber > giapSuccess) {
                const updateKnb = user.knb - giapAmount
                Users.update({userId: authorId}, {knb: updateKnb})
                return msg.reply('Nâng cấp thất bại, xin chia buồn cùng thống lĩnh')
            }
            const upgiap = Users.update({userId: authorId}, {giap: nextGiap, knb: (user.knb - giapAmount)})
            msg.reply(`Chúc mừng thống lĩnh nâng giáp thành công lên cấp [${nextGiap}]`).then()
            break;

        case "uppet":
            if (nextPet > currentLevel) return msg.reply('Không thể nâng vượt cấp của thống lĩnh')
            if (petAmount > currentKnb) return msg.reply(`Thống lĩnh không đủ knb (thiếu ${petAmount - parseInt(currentKnb)})`).then();
            if (randomNumber > petSuccess) {
                const updateKnb = user.knb - petAmount
                Users.update({userId: authorId}, {knb: updateKnb})
                return msg.reply('Nâng cấp thất bại, xin chia buồn cùng thống lĩnh')
            }
            const uppet = Users.update({userId: authorId}, {pet: nextPet, knb: (user.knb - petAmount)})
            msg.reply(`Chúc mừng thống lĩnh nâng Pet thành công lên cấp [${nextPet}]`).then()
            break;

        case "upthanbinh":
            if (nextThanbinh > currentLevel) return msg.reply('Không thể nâng vượt cấp của thống lĩnh')
            if (thanbinhAmount > currentKnb) return msg.reply(`Thống lĩnh không đủ knb (thiếu ${thanbinhAmount - parseInt(currentKnb)})`).then();
            if (randomNumber > thanbinhSuccess) {
                const updateKnb = user.knb - thanbinhAmount
                Users.update({userId: authorId}, {knb: updateKnb})
                return msg.reply('Nâng cấp thất bại, xin chia buồn cùng thống lĩnh')
            }
            const upthanbinh = Users.update({userId: authorId}, {thanbinh: nextThanbinh, knb: (user.knb - thanbinhAmount)})
            msg.reply(`Chúc mừng thống lĩnh nâng thần binh thành công lên cấp [${nextThanbinh}]`).then()
            break;

        default:
            msg.reply('Sai comment').then();
            break;
    }

});

client.login(config.DISCORD_BOT_TOKEN).then();
const API_URL = process.env.SALESFORCE_URL || 'https://cainz--indev6.my.salesforce.com/services/apexrest';

module.exports.config = {
  BASE_URL: process.env.BASE_URL || 'localhost',
  NODE_ENV: process.env.NODE_ENV || 'development',
  SERVER_PORT: process.env.PORT || 8080,

  DISCORD_BOT_TOKEN: 'NzcxMTc4OTI0NDI3NzA2NDI4.X5oWVw.qacKirplzbh8yQQ1HtgzC_SdLrk',

  GENERAL_CHANNEL_ID: '770958949062672396',
  TEST_BOT_CHANNEL_ID: '771192394595041291',

  //GAME
  level: {
    0: {
      knb: 0
    },
    1: {
      knb: 100,
      sucmanh: 15,
      phongthu: 15,
      tranhne: 15,
      tile: 100
    },
    2: {
      knb: 200,
      sucmanh: 25,
      phongthu: 25,
      tranhne: 20,
      tile: 80
    },
    3: {
      knb: 500,
      sucmanh: 35,
      phongthu: 35,
      tranhne: 30,
      tile: 70
    },
    4: {
      knb: 1000,
      sucmanh: 45,
      phongthu: 45,
      tranhne: 40,
      tile: 60
    },
    5: {
      knb: 1500,
      sucmanh: 55,
      phongthu: 55,
      tranhne: 50,
      tile: 50
    },
    6: {
      knb: 2000,
      sucmanh: 65,
      phongthu: 65,
      tranhne: 60,
      tile: 40
    },
    7: {
      knb: 2500,
      sucmanh: 75,
      phongthu: 75,
      tranhne: 70,
      tile: 40
    },
    8: {
      knb: 3000,
      sucmanh: 85,
      phongthu: 85,
      tranhne: 80,
      tile: 40
    },
    9: {
      knb: 3500,
      sucmanh: 95,
      phongthu: 95,
      tranhne: 90,
      tile: 30
    },
    10: {
      knb: 4000,
      sucmanh: 200,
      phongthu: 150,
      tranhne: 100,
      tile: 20
    },
  },

  thanbinh: {
    0: {
      knb: 0
    },
    1: {
      knb: 500,
      sucmanh: 100,
      tile: 90
    },
    2: {
      knb: 1000,
      sucmanh: 200,
      tile: 75
    },
    3: {
      knb: 1500,
      sucmanh: 300,
      tile: 60
    },
    4: {
      knb: 2000,
      sucmanh: 400,
      tile: 50
    },
    5: {
      knb: 3000,
      sucmanh: 550,
      tile: 40
    },
    6: {
      knb: 4000,
      sucmanh: 700,
      tile: 30
    },
    7: {
      knb: 6000,
      sucmanh: 900,
      tile: 20
    },
    8: {
      knb: 8000,
      sucmanh: 1100,
      tile: 15
    },
    9: {
      knb: 10000,
      sucmanh: 1300,
      tile: 10
    },
    10: {
      knb: 15000,
      sucmanh: 2000,
      tile: 5
    }
  },

  giap: {
    0: {
      knb: 0
    },
    1: {
      knb: 200,
      phongthu: 50,
      tile: 100
    },
    2: {
      knb: 400,
      phongthu: 100,
      tile: 90
    },
    3: {
      knb: 600,
      phongthu: 150,
      tile: 80
    },
    4: {
      knb: 800,
      phongthu: 200,
      tile: 70
    },
    5: {
      knb: 1000,
      phongthu: 300,
      tile: 60
    },
    6: {
      knb: 2000,
      phongthu: 400,
      tile: 50
    },
    7: {
      knb: 3000,
      phongthu: 600,
      tile: 50
    },
    8: {
      knb: 4000,
      phongthu: 800,
      tile: 40
    },
    9: {
      knb: 5000,
      phongthu: 1000,
      tile: 30
    },
    10: {
      knb: 7000,
      phongthu: 1500,
      tile: 20
    }
  },

  pet: {
    0: {
      knb: 0
    },
    1: {
      knb: 100,
      netranh: 50,
      tile: 100
    },
    2: {
      knb: 200,
      netranh: 100,
      tile: 90
    },
    3: {
      knb: 300,
      netranh: 150,
      tile: 80
    },
    4: {
      knb: 600,
      netranh: 200,
      tile: 70
    },
    5: {
      knb: 800,
      netranh: 300,
      tile: 60
    },
    6: {
      knb: 1000,
      netranh: 400,
      tile: 50
    },
    7: {
      knb: 1500,
      netranh: 600,
      tile: 50
    },
    8: {
      knb: 2000,
      netranh: 800,
      tile: 40
    },
    9: {
      knb: 4000,
      netranh: 1000,
      tile: 30
    },
    10: {
      knb: 6000,
      netranh: 1500,
      tile: 20
    }
  },
}

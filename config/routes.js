const express = require('express');
const appRouter = express.Router();
const { config } = require('./config')

const Discord = require('discord.js');
const client = new Discord.Client();


appRouter.get('/version', async (req, res) => {
    res.status(200).send('Hello, this is SPI version 1.0')
})

appRouter.get('/bot', async (req, res) => {
    const commandPrefix = "!";

    client.on('ready', () => {
        console.log(`Logged in as ${client.user.tag}!`);
    });

    client.on('message', msg => {
        if(!msg.content.startsWith(commandPrefix)) {
            if (msg.content.toLowerCase().includes('chào hân')) {
                msg.reply('Chào thống lĩnh ạ');
            }
            return
        }
        const args = msg.content.slice(commandPrefix.length).trim().split(/ +/g);
        const command = args.shift().toLowerCase();

        switch(command){
            case "ping":
                msg.reply('Pong!');
                break;
            default:
                msg.reply('Sai comment');
                break;
        }

    });

    client.login(config.DISCORD_BOT_TOKEN);



    res.status(200).send('Bot running')
})

module.exports = appRouter

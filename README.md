# Dogrun_back

## Prerequisite
- Install Google Cloud SDK: [https://cloud.google.com/sdk/docs/install](https://cloud.google.com/sdk/docs/install)
- NodeJS v12 and `npm`: [https://nodejs.org/en/download/](https://nodejs.org/en/download)

## Installation and Usage

1. Install node dependencies
```
npm install
```

2. Prepare the environment variables
```
cp app.env .env
```

3. localhost:8080:
```
npm run dev
```

## Deployment
1. ```gcloud init```: Initialize, authorize, and configure the gcloud tool (1st use)

2. App Engine Service deployment command:
- ```npm run deploy:dev``` => Development: dev-wonqol-pj.an.r.appspot.com
- ```npm run deploy:staging``` => Staging: stg-wanqol-pj.an.r.appspot.com
- ```npm run deploy:production``` => Production

3. App Engine Dispatcher deployment command
- ```npm run deploy:dispatch:dev``` => Dispatch all /api request to backend service
- ```npm run deploy:dispatch:staging``` => Dispatch all /api request to backend service
- ```npm run deploy:dispatch:production``` => Dispatch all /api request to backend service and production domain mapping

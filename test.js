// Initial configuration variables
require("dotenv").config();

const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");

var originsWhitelist = [
    "http://localhost:4200",
    process.env.FRONTEND_URL
];

var corsOptions = {
    origin: function (origin, callback) {
        var isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
        callback(null, isWhitelisted);
    },
    credentials: true,
};


// Initial application context
const app = express();

// parse requests of content-type: application/json
app.use(bodyParser.json());

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// Apply CORS
app.use(cors(corsOptions));

// Default path
app.get("/", (req, res) => {
    res.json({ message: "common-service-GMO-backend" });
});

// Custom routes
require("./app/routes/saleReport.route")(app);

// set port, listen for requests
const port = process.env.PORT || '8080'
app.listen(port, () => {
    console.log(`App listening on host: http://localhost:${port}`);
});